import React from 'react';
import {StyleSheet, View} from 'react-native';
import Animated, {interpolate, useDerivedValue} from 'react-native-reanimated';
import {ReText, Vector, round, Path} from 'react-native-redash';

import {SIZE} from './Model';

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  values: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  value: {
    fontWeight: '500',
    fontSize: 24,
  },
  label: {
    fontSize: 18,
  },
});

interface HeaderProps {
  translation: Vector<Animated.SharedValue<number>>;
  graph: {minY: number; maxY: number; path: Path};
}

const Header = ({translation, graph}: HeaderProps) => {
  const data = useDerivedValue(() => graph);
  const price = useDerivedValue(() => {
    const p = interpolate(
      translation.y.value,
      [0, SIZE],
      // @ts-ignore
      [data.maxY, data.minY],
    );
    return `${round(p, 2)}`;
  });

  return (
    <View style={styles.container}>
      <View style={styles.values}>
        <View>
          <ReText style={styles.value} text={price} />
        </View>
      </View>
    </View>
  );
};

export default Header;
