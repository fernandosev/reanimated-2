/* eslint-disable camelcase */
import * as shape from 'd3-shape';
import {scaleLinear} from 'd3-scale';
import {Dimensions} from 'react-native';
import {parse} from 'react-native-redash';

// import data from './data.json';

export const SIZE = Dimensions.get('window').width;

const buildGraph = (datapoints: number[][]) => {
  const priceList = datapoints;
  const prices = priceList.map((value) => value[0]);
  const dates = priceList.map((value) => value[1]);
  const scaleX = scaleLinear()
    .domain([Math.min(...dates), Math.max(...dates)])
    .range([0, SIZE]);
  const minY = Math.min(...prices);
  const maxY = Math.max(...prices);
  const scaleY = scaleLinear().domain([minY, maxY]).range([SIZE, 0]);
  return {
    minY,
    maxY,
    path: parse(
      shape
        .line()
        .x(([, x]) => scaleX(x) as number)
        .y(([y]) => scaleY(y) as number)
        .curve(shape.curveBasis)(priceList) as string,
    ),
  };
};

export default buildGraph;
