import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Text,
} from 'react-native';
import Svg, {Path} from 'react-native-svg';
import Animated, {
  useAnimatedProps,
  useSharedValue,
} from 'react-native-reanimated';
import {mixPath, useVector} from 'react-native-redash';

import buildGraph, {SIZE} from './Model';
import Header from './Header';
import Cursor from './Cursor';
import {useEffect} from 'react';
import {useRef} from 'react';
import {useState} from 'react';

const {width} = Dimensions.get('window');
const AnimatedPath = Animated.createAnimatedComponent(Path);

const SELECTION_WIDTH = width - 32;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  selection: {
    flexDirection: 'row',
    width: SELECTION_WIDTH,
    alignSelf: 'center',
  },
  label: {
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

// Array [x, y] -> (number, timestamp)
const data = [
  [10365.763144, 1599119580],
  [10354.38030756, 1599119520],
  [10349.18557836, 1599119460],
  [10346.1234222, 1599119400],
  [10346.88896124, 1599119340],
  [10344.2551424, 1599119280],
  [10348.25599524, 1599119220],
  [10348.34713084, 1599119160],
  [10348.28333592, 1599119100],
  [10347.37197992, 1599119040],
  [10347.01655108, 1599118980],
  [10340.39099296, 1599118920],
  [10340.9742608, 1599118860],
  [10332.64446696, 1599118800],
  [10334.5036332, 1599118740],
  [10332.64446696, 1599118680],
  [10340.58237772, 1599118620],
  [10340.58237772, 1599118560],
  [10340.58237772, 1599118500],
  [10340.5641506, 1599118440],
];

const Graph = () => {
  const translation = useVector();
  const transition = useSharedValue(0);

  const [graph, setGraph] = useState(buildGraph(data));

  //console.log(graph);

  const animatedProps = useAnimatedProps(() => {
    const previousPath = graph.path;
    const currentPath = graph.path;
    return {
      d: mixPath(transition.value, previousPath, currentPath),
    };
  });

  const time = useRef(1599119580);
  // const flagAux = useRef(false);
  // const [flag, setFlag] = useState(flagAux);

  // useEffect(() => {
  //   const realTime = setInterval(() => {
  //     time.current = time.current + 20;
  //     data.unshift([Math.random() * (10350 - 10300) + 10300, time.current]);
  //     graph.value = buildGraph(data);
  //     // setFlag(!flagAux.current);
  //     // flagAux.current = !flagAux.current;
  //   }, 1000);

  //   return () => clearInterval(realTime);
  // }, [graph]);

  return (
    <View style={styles.container}>
      <Header translation={translation} graph={graph} />
      <View>
        <Svg width={SIZE} height={SIZE}>
          <AnimatedPath
            animatedProps={animatedProps}
            fill="transparent"
            stroke="black"
            strokeWidth={3}
          />
        </Svg>
        <Cursor translation={translation} graph={graph} />
      </View>

      <TouchableOpacity
        onPress={() => {
          time.current = time.current + 20;
          data.unshift([Math.random() * (10350 - 10300) + 10300, time.current]);
          setGraph(buildGraph(data));
        }}
        style={{
          backgroundColor: 'purple',
          width: 200,
          height: 80,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text style={{color: 'white', fontSize: 20, fontWeight: 'bold'}}>
          Click Me!
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Graph;
