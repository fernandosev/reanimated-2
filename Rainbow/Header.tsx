import React from 'react';
import {StyleSheet, View} from 'react-native';
import Animated, {interpolate, useDerivedValue} from 'react-native-reanimated';
import {ReText, Vector, round} from 'react-native-redash';

import {graphs, SIZE, GraphIndex} from './Model';

const styles = StyleSheet.create({
  container: {
    padding: 16,
  },
  values: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  value: {
    fontWeight: '500',
    fontSize: 24,
  },
  label: {
    fontSize: 18,
  },
});

interface HeaderProps {
  translation: Vector<Animated.SharedValue<number>>;
  index: Animated.SharedValue<GraphIndex>;
}

const Header = ({translation, index}: HeaderProps) => {
  const data = useDerivedValue(() => graphs[index.value].data);
  const price = useDerivedValue(() => {
    const p = interpolate(
      translation.y.value,
      [0, SIZE],
      [data.value.maxPrice, data.value.minPrice],
    );
    return `${round(p, 2).toLocaleString('en-US', {currency: 'USD'})}`;
  });
  return (
    <View style={styles.container}>
      <View style={styles.values}>
        <View>
          <ReText style={styles.value} text={price} />
        </View>
      </View>
    </View>
  );
};

export default Header;
