import React, {useImperativeHandle} from 'react';
import {View, StyleSheet} from 'react-native';
import {PanGestureHandler} from 'react-native-gesture-handler';
import Animated, {
  useAnimatedGestureHandler,
  useSharedValue,
  useAnimatedStyle,
  runOnJS,
} from 'react-native-reanimated';

import {Vector} from 'react-native-redash';

// import {graphs} from './Model';

const CURSOR = 50;
const styles = StyleSheet.create({
  cursor: {
    width: CURSOR,
    height: CURSOR,
    borderRadius: CURSOR / 2,
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
  },
  cursorBody: {
    width: 15,
    height: 15,
    borderRadius: 7.5,
    backgroundColor: 'black',
  },
  cursorLine: {
    width: 2,
    height: 290,
    backgroundColor: 'red',
  },
});

interface CursorProps {
  translation: Vector<Animated.SharedValue<number>>;
  data: number[];
  SIZE: number;
  setCursorIndex?: (number: number | undefined) => void;
}

interface ICursorHandle {
  getCursorIndex(): number | undefined;
}

const Cursor = React.forwardRef<ICursorHandle, CursorProps>(
  ({translation, data, SIZE, setCursorIndex = () => {}}, ref) => {
    useImperativeHandle(ref, () => ({
      getCursorIndex: () => cursorIndex.value,
    }));

    const isActive = useSharedValue(false);
    const cursorPositionX = useSharedValue<number | undefined>(undefined);
    const cursorIndex = useSharedValue<number | undefined>(undefined);

    const onGestureEvent = useAnimatedGestureHandler({
      onStart: () => {
        isActive.value = true;
      },
      onActive: (event) => {
        const index = Math.trunc(event.x / (SIZE / data.length));
        translation.x.value = event.x;
        translation.y.value = event.y;

        cursorPositionX.value = event.x;

        if (index !== cursorIndex.value) {
          cursorIndex.value = index;
          runOnJS(setCursorIndex)(index);
        }
      },
      onEnd: () => {
        isActive.value = false;
        cursorPositionX.value = undefined;
        cursorIndex.value = undefined;
        runOnJS(setCursorIndex)(undefined);
      },
    });

    const style = useAnimatedStyle(() => {
      const translateX = translation.x.value - 2;
      return {
        transform: [{translateX}, {scale: isActive.value ? 1 : 0}],
      };
    });

    return (
      <View style={StyleSheet.absoluteFill}>
        <PanGestureHandler {...{onGestureEvent}}>
          <Animated.View style={StyleSheet.absoluteFill}>
            <Animated.View style={[styles.cursorLine, style, {height: SIZE}]} />
          </Animated.View>
        </PanGestureHandler>
      </View>
    );
  },
);

export default Cursor;
