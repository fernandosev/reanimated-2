import React, {useCallback} from 'react';
import {View, StyleSheet, Dimensions} from 'react-native';
import {Circle} from 'react-native-svg';
import {LineChart, XAxis, YAxis, Grid} from 'react-native-svg-charts';
import {useSharedValue} from 'react-native-reanimated';
import {useVector} from 'react-native-redash';

import {GraphIndex, SIZE} from './Model';
import Cursor from './Cursor';
import {useRef} from 'react';
import {useState} from 'react';

const {width} = Dimensions.get('window');

const SELECTION_WIDTH = width - 32;
const BUTTON_WIDTH = width - 32;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  backgroundSelection: {
    backgroundColor: '#f3f3f3',
    ...StyleSheet.absoluteFillObject,
    width: BUTTON_WIDTH,
    borderRadius: 8,
  },
  selection: {
    flexDirection: 'row',
    width: SELECTION_WIDTH,
    alignSelf: 'center',
  },
  labelContainer: {
    padding: 16,
    width: BUTTON_WIDTH,
  },
  label: {
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

const Graph = () => {
  const cursorRef = useRef<any>(null);
  const translation = useVector();
  const current = useSharedValue<GraphIndex>(0);
  const [cursorIndex, setCursorIndex] = useState<number>(undefined);

  const data = [
    0,
    50,
    10,
    40,
    95,
    -4,
    -24,
    85,
    100,
    35,
    53,
    -53,
    24,
    50,
    -50,
    -100,
  ];

  // useEffect(() => {
  //   if (cursorIndex !== undefined) {
  //     console.log(data[cursorIndex]);
  //     setActivePoints([
  //       {x: cursorIndex, y: data[cursorIndex], color: 'rgb(134, 65, 244)'},
  //     ]);
  //   }
  // }, [cursorIndex]);

  const Decorator = useCallback(
    ({x, y, data}: {x: any; y: any; data: number[]}) => {
      return (
        <>
          {cursorIndex !== undefined && (
            <Circle
              cx={x(cursorIndex)}
              cy={y(data[cursorIndex])}
              r={4}
              stroke="rgb(134, 65, 244)"
              strokeWidth={2}
              fill="white"
            />
          )}
        </>
      );
    },
    [cursorIndex],
  );

  return (
    <View style={styles.container}>
      {/* <Header translation={translation} index={current} /> */}
      <View style={{width: SIZE, height: SIZE}}>
        <LineChart
          style={{height: SIZE}}
          data={data}
          svg={{stroke: 'rgb(134, 65, 244)', strokeWidth: 3}}>
          <Grid />
          <Decorator x={2} y={2} data={data} />
        </LineChart>

        <XAxis
          style={{marginHorizontal: -10}}
          data={data}
          formatLabel={(value, index) => index}
          contentInset={{left: 10, right: 10}}
          svg={{fontSize: 10, fill: 'black'}}
        />

        <Cursor
          ref={cursorRef}
          data={data}
          translation={translation}
          SIZE={SIZE}
          setCursorIndex={setCursorIndex}
        />
      </View>
    </View>
  );
};

export default Graph;
